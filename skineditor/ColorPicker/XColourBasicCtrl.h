#if !defined(X_COLORBASIC_H)
#define X_COLORBASIC_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "CXRect.h"
#include "CXDC.h"


extern UINT WM_XCOLORBASIC_SELCHANGE;
/////////////////////////////////////////////////////////////////////////////

class XColorBasic
{
// Construction
public:
    XColorBasic();
	virtual ~XColorBasic();

    void Initialise();

// Attributes
public:
	HWND m_hWnd;

// Operations
public:
	BOOL Create(HINSTANCE hInstance, DWORD dwStyle, const RECT& rect,
		HWND hParent, UINT nID, COLORREF crInitialColor = RGB(0, 0, 0) );

	COLORREF GetRGB();

	TCHAR * GetHex(TCHAR * hexbuf);

// Implementation
public:
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

protected:
    BOOL GetCellRect(int nIndex, const LPRECT& rect);
	int GetCellAtPoint(POINT pt);

    void FindCellFromColour(COLORREF crColour);

    void DrawCell(CXDC* pDC, int nIndex);

	void DrawCells(CXDC * pDC);

	void SendColorToParent(UINT nMessage, COLORREF cr);

	COLORREF GetColour(int nIndex) { return m_crColours[nIndex]; }
    int  GetIndex(int row, int col) const;
    int  GetRow(int nIndex) const;
    int  GetColumn(int nIndex) const;

// protected attributes
protected:
    static COLORREF m_crColours[];
    int            m_nNumColours;
    int            m_nNumColumns, m_nNumRows;
	int            m_nBoxSizeW, m_nBoxSizeH;
	int            m_nMarginX, m_nMarginY;
	int            m_nCurrentSel;

	UINT		   m_nDlgCode;
    COLORREF       m_crInitialColour, m_crColour;
    CToolTipCtrl   m_ToolTip;
	HWND		   m_hParent;
	CXRect		   m_rectCtrl, m_rectPaint;
};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(X_COLORBASIC_H)
