// С�� 2017.8

#include "stdafx.h"
#include <math.h>
#include "XColourBasicCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define INVALID_COLOUR    -1

#define MAX_COLOURS      100
#define EDGE_WIDTH		 2

COLORREF XColorBasic::m_crColours[] = 
{
	{ RGB(0xff, 0x80, 0x80) },
	{ RGB(0xff, 0xff, 0x80) },
	{ RGB(0x80, 0xff, 0x80) },
	{ RGB(0x00, 0xff, 0x80) },
	{ RGB(0x80, 0xff, 0xff) },
	{ RGB(0x00, 0x80, 0xff) },
	{ RGB(0xff, 0x80, 0xc0) },
	{ RGB(0xff, 0x80, 0xff) },

	{ RGB(0xff, 0x00, 0x00) },
	{ RGB(0xff, 0xff, 0x00) },
	{ RGB(0x80, 0xff, 0x00) },
	{ RGB(0x00, 0xff, 0x40) },
	{ RGB(0x00, 0xff, 0xff) },
	{ RGB(0x00, 0x80, 0xc0) },
	{ RGB(0x80, 0x80, 0xc0) },
	{ RGB(0xff, 0x00, 0xff) },

	{ RGB(0x80, 0x40, 0x40) },
	{ RGB(0xff, 0x80, 0x40) },
	{ RGB(0x00, 0xff, 0x00) },
	{ RGB(0x00, 0x80, 0x80) },
	{ RGB(0x00, 0x40, 0x80) },
	{ RGB(0x80, 0x80, 0xff) },
	{ RGB(0x80, 0x00, 0x40) },
	{ RGB(0xff, 0x00, 0x80) },

	{ RGB(0x80, 0x00, 0x00) },
	{ RGB(0xff, 0x80, 0x00) },
	{ RGB(0x00, 0x80, 0x00) },
	{ RGB(0x00, 0x80, 0x40) },
	{ RGB(0x00, 0x00, 0xff) },
	{ RGB(0x00, 0x00, 0xa0) },
	{ RGB(0x80, 0x00, 0x80) },
	{ RGB(0x80, 0x00, 0xff) },

	{ RGB(0x40, 0x00, 0x00) },
	{ RGB(0x80, 0x40, 0x00) },
	{ RGB(0x00, 0x40, 0x00) },
	{ RGB(0x00, 0x40, 0x40) },
	{ RGB(0x00, 0x00, 0x80) },
	{ RGB(0x00, 0x00, 0x40) },
	{ RGB(0x40, 0x00, 0x40) },
	{ RGB(0x40, 0x00, 0x80) },

	{ RGB(0x00, 0x00, 0x00) },
	{ RGB(0x80, 0x80, 0x00) },
	{ RGB(0x80, 0x80, 0x40) },
	{ RGB(0x80, 0x80, 0x80) },
	{ RGB(0x40, 0x80, 0x80) },
	{ RGB(0xc0, 0xc0, 0xc0) },
	{ RGB(0x40, 0x00, 0x40) },
	{ RGB(0xff, 0xff, 0xff) }

};

UINT WM_XCOLORBASIC_SELCHANGE = ::RegisterWindowMessage(_T("WM_XCOLORBASIC_SELCHANGE"));

/////////////////////////////////////////////////////////////////////////////
// XColorBasic

XColorBasic::XColorBasic()
	:m_nDlgCode(DLGC_WANTARROWS | DLGC_WANTTAB)
{
	Initialise();
}

void XColorBasic::Initialise()
{
    m_nNumColours       = sizeof(m_crColours)/sizeof(COLORREF);
    ASSERT(m_nNumColours <= MAX_COLOURS);
    if (m_nNumColours > MAX_COLOURS)
        m_nNumColours = MAX_COLOURS;

    m_nNumColumns       = 0;
    m_nNumRows          = 0;
	m_nBoxSizeW = 18;
	m_nBoxSizeH = 18;
	m_nMarginX = ::GetSystemMetrics(SM_CXEDGE);
	m_nMarginY = ::GetSystemMetrics(SM_CYEDGE);
	m_nCurrentSel = INVALID_COLOUR;
    m_hParent           = NULL;
    m_crColour          = m_crInitialColour = RGB(0,0,0);
	m_nMarginX = m_nMarginY = 8;

	m_nNumColumns = 8;
	m_nNumRows = m_nNumColours / m_nNumColumns;
	if (m_nNumColours % m_nNumColumns) m_nNumRows++;
}

XColorBasic::~XColorBasic()
{

}

/////////////////////////////////////////////////////////////////////////////
// CColourPopup message handlers
static
LRESULT __stdcall DefWindowProcX(HWND hWnd,		// handle to window
	UINT message,	// message identifier
	WPARAM wParam,	// first message parameter
	LPARAM lParam)	// second message parameter
{
	switch (message)
	{
	case WM_CREATE:
	{
		// save 'this' pointer in windows extra memory - the lParam
		// is set when ::CreateWindowEx() is called
		CREATESTRUCT* pcs = (CREATESTRUCT *)lParam;
		if (!pcs)
		{
			TRACE(_T("ERROR - CREATESTRUCT lParam is zero\n"));
			_ASSERTE(pcs);
			return -1;		// abort creation
		}
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)pcs->lpCreateParams);
		return 0;
	}
	break;

	default:
	{
		// dispatch via saved 'this' pointer
		LONG_PTR lData = ::GetWindowLongPtr(hWnd, GWLP_USERDATA);
		if (lData)
		{
			XColorBasic *pCtrl = (XColorBasic *)lData;
			return pCtrl->WindowProc(message, wParam, lParam);
		}
		else
		{
			// probably some WM_NCxxxx message
			TRACE(_T("GWLP_USERDATA = 0 for message = 0x%04X\n"), message);
		}
	}
	break;
	}

	return ::DefWindowProc(hWnd, message, wParam, lParam);
}


BOOL XColorBasic::Create(HINSTANCE hInstance, DWORD dwStyle, const RECT & rect, HWND hParent, UINT nID, COLORREF crColour)
{
	m_hParent = hParent;
	m_crColour = m_crInitialColour = crColour;

	_ASSERTE(IsWindow(m_hParent));
	if (!IsWindow(m_hParent))
		return FALSE;

	m_rectCtrl = rect;
	m_rectPaint = rect;

	TCHAR * pszClassName = _T("XColorBasicCtrl");

	WNDCLASS wc =
	{
		CS_DBLCLKS,									// class style - want WM_LBUTTONDBLCLK messages
		DefWindowProcX,								// window proc
		0,											// class extra bytes
		0,											// window extra bytes
		hInstance,									// instance handle
		0,											// icon
		::LoadCursor(0, IDC_ARROW),					// cursor
		0,											// background brush
		0,											// menu name
		pszClassName								// class name
	};

	if (!::RegisterClass(&wc))
	{
		DWORD dwLastError = GetLastError();
		if (dwLastError != ERROR_CLASS_ALREADY_EXISTS)
		{
			TRACE(_T("ERROR - RegisterClass failed, GetLastError() returned %u\n"),
				dwLastError);
			_ASSERTE(FALSE);
			return FALSE;
		}
	}

	// we pass 'this' pointer as lpParam, so DefWindowProcX will see it 
	// in WM_CREATE message
	m_hWnd = ::CreateWindowEx(0, pszClassName, _T(""), dwStyle,
		m_rectCtrl.left, m_rectCtrl.top, m_rectCtrl.Width(), m_rectCtrl.Height(),
		hParent, (HMENU)nID, hInstance, this);

	m_rectPaint.DeflateRect(2* EDGE_WIDTH, 2* EDGE_WIDTH);
	m_nBoxSizeW = (m_rectPaint.Width() - (m_nNumColumns - 1) * m_nMarginX) / m_nNumColumns;
	m_nBoxSizeH = (m_rectPaint.Height() - (m_nNumRows - 1) * m_nMarginY) / m_nNumRows;


	// Create the tooltips
	//CreateToolTips();

	// Find which cell (if any) corresponds to the initial colour
	FindCellFromColour(crColour);

	return m_hWnd != 0;
}

COLORREF XColorBasic::GetRGB()
{
	return m_crColour;
}

TCHAR* XColorBasic::GetHex(TCHAR* hexbuf)
{
	if (m_crColour == INVALID_COLOUR)
		return hexbuf;

	sprintf(hexbuf, _T("%02x%02x%02x"), (int)GetRValue(m_crColour), (int)GetGValue(m_crColour), (int)GetBValue(m_crColour));
	return hexbuf;
}

/////////////////////////////////////////////////////////////////////////////
// CColourPopup implementation

int XColorBasic::GetIndex(int row, int col) const
{ 
	if (row < 0 || col < 0 || row >= m_nNumRows || col >= m_nNumColumns)
        return INVALID_COLOUR;
    else
    {
        if (row*m_nNumColumns + col >= m_nNumColours)
            return INVALID_COLOUR;
        else
            return row*m_nNumColumns + col;
    }
}

int XColorBasic::GetRow(int nIndex) const               
{ 
	if (nIndex < 0 || nIndex >= m_nNumColours)
        return INVALID_COLOUR;
    else
        return nIndex / m_nNumColumns; 
}

int XColorBasic::GetColumn(int nIndex) const            
{ 
    if (nIndex < 0 || nIndex >= m_nNumColours)
        return INVALID_COLOUR;
    else
        return nIndex % m_nNumColumns; 
}

void XColorBasic::FindCellFromColour(COLORREF crColour)
{
    for (int i = 0; i < m_nNumColours; i++)
    {
        if (GetColour(i) == crColour)
        {
			m_nCurrentSel = i;
            return;
        }
    }

	m_nCurrentSel = INVALID_COLOUR;
}

// Gets the dimensions of the colour cell given by (row,col)
BOOL XColorBasic::GetCellRect(int nIndex, const LPRECT& rect)
{
    if (nIndex < 0 || nIndex >= m_nNumColours)
        return FALSE;

    rect->left = GetColumn(nIndex) * m_nBoxSizeW + GetColumn(nIndex) *m_nMarginX + EDGE_WIDTH;
    rect->top  = GetRow(nIndex) * m_nBoxSizeH + GetRow(nIndex) *m_nMarginY + EDGE_WIDTH;

    rect->right = rect->left + m_nBoxSizeW;
    rect->bottom = rect->top + m_nBoxSizeH;

    return TRUE;
}

int XColorBasic::GetCellAtPoint(POINT pt)
{
	pt.x -= EDGE_WIDTH;
	pt.y -= EDGE_WIDTH;
	int x = pt.x / (m_nBoxSizeW + m_nMarginX);
	int y = pt.y / (m_nBoxSizeH + m_nMarginY);
	if ((pt.x > ((x+1)*m_nBoxSizeW + x*m_nMarginX))
		|| (pt.y > ((y+1)*m_nBoxSizeH + y*m_nMarginY)))
	{
		return -1;
	}
	return y * m_nNumColumns + x;
}

void XColorBasic::DrawCell(CXDC* pDC, int nIndex)
{
    CXRect rect;
    if (!GetCellRect(nIndex, rect)) return;

	// Draw button
	pDC->DrawEdge(rect, BDR_SUNKENOUTER, BF_RECT);

	rect.DeflateRect(1, 1);
    // fill background
	pDC->FillSolidRect(rect, GetColour(nIndex));// ::GetSysColor(COLOR_3DFACE));

	if (m_nCurrentSel == nIndex)
	{
		rect.DeflateRect(-3, -3);
		pDC->Draw_Frame(rect, RGB(0, 0, 0), 1, PS_DOT);
	}
}

void XColorBasic::DrawCells(CXDC* pDC)
{
	CXRect rect = m_rectCtrl;
	rect.OffsetRect(-rect.left, -rect.top);
	pDC->FillSolidRect(rect, ::GetSysColor(COLOR_3DFACE));

	// Draw colour cells
	for (int i = 0; i < m_nNumColours; i++)
		DrawCell(pDC, i);
}

void XColorBasic::SendColorToParent(UINT nMessage, COLORREF cr)
{
	// don't send duplicate messages
	//if ((nMessage == WM_XCOLORPICKER_SELENDOK) || (cr != m_crLastSent))
	{
		m_crColour = cr;
		if (IsWindow(m_hParent) && (cr != NO_COLOR))
		{
			TRACE(_T(">>>>> sending %06X\n"), m_crColour);
			::SendMessage(m_hParent, nMessage,
				m_crColour, ::GetDlgCtrlID(m_hWnd));
		}
	}
}


LRESULT XColorBasic::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		// will never see this message - it's handled by DefWindowProcX
		break;

	case WM_ERASEBKGND:
		// nothing to erase, since we draw the entire client area
		return TRUE;

	case WM_PAINT:
	{
		TRACE(_T("in WM_PAINT\n"));
		PAINTSTRUCT ps;
		HDC hdc = (wParam != NULL) ? (HDC)wParam : ::BeginPaint(m_hWnd, &ps);
		if (hdc == 0)
			return 0;
		CXDC dc(hdc);
		
		DrawCells(&dc);
		
		if (wParam == NULL)
			::EndPaint(m_hWnd, &ps);
		return 0;
	}

	case WM_GETDLGCODE:
		return m_nDlgCode;
		break;

	case WM_LBUTTONDOWN:
	{
		POINT point;
		point.x = GET_X_LPARAM(lParam);
		point.y = GET_Y_LPARAM(lParam);
		TRACE(_T("in WM_LBUTTONDOWN:  point.x=%d  point.y=%d\n"), point.x, point.y);

		m_nCurrentSel = GetCellAtPoint(point);
		if (m_nCurrentSel != -1)
		{
			CXDC dc(m_hWnd);
			DrawCells(&dc);

			m_crColour = m_crColours[m_nCurrentSel];
			SendColorToParent(WM_XCOLORBASIC_SELCHANGE, m_crColour);
		}
		else
			m_crColour = -1;

		break;
	}

	case WM_LBUTTONUP:
		TRACE(_T("in WM_LBUTTONUP\n"));
		break;

	case WM_LBUTTONDBLCLK:
	{
		POINT point;
		point.x = GET_X_LPARAM(lParam);
		point.y = GET_Y_LPARAM(lParam);
		break;
	}

	case WM_MOUSEMOVE:
	{
		POINT point;
		point.x = GET_X_LPARAM(lParam);
		point.y = GET_Y_LPARAM(lParam);
		break;
	}

	}

	return ::DefWindowProc(m_hWnd, message, wParam, lParam);
}