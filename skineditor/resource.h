//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 skineditor.rc 使用
//
#define IDR_MANIFEST                    1
#define IDD_ABOUTBOX                    100
#define IDD_SKINEDITOR_FORM             101
#define IDR_MAINFRAME                   128
#define IDR_SKINEDTYPE                  129
#define IDD_COLORPICK                   131
#define IDB_FLAG                        132
#define IDD_BMPCOMBINE                  133
#define IDD_PANEL_VERT                  134
#define IDD_PANEL_PREV                  135
#define IDD_COLOR_PICKER                137
#define IDC_VALUE                       1001
#define IDC_PREV                        1002
#define IDC_IMAGEPREV                   1004
#define IDC_SP_INPUTWND                 1005
#define IDC_SP_STATUSSHRINK             1006
#define IDC_SP_STATUSFULL               1007
#define IDC_TAB_MODE                    1008
#define IDC_HOME                        1010
#define IDC_BM_WHITE                    1011
#define IDC_BM_BLACK                    1012
#define IDC_BITMAP1                     1012
#define IDC_BM_TRANSGRID                1013
#define IDC_BROWSE1                     1013
#define IDC_BITMAP2                     1014
#define IDC_BROWSE2                     1015
#define IDC_BITMAP3                     1016
#define IDC_BROWSE3                     1017
#define IDC_BITMAP4                     1018
#define IDC_BROWSE4                     1019
#define IDC_COMBINE                     1020
#define IDC_SP_IPTWNDVERT               1021
#define IDC_FRAME                       1023
#define IDC_COLOR_R                     1024
#define IDC_COLOR_G                     1025
#define IDC_COLOR_B                     1026
#define IDC_COLOR_H                     1027
#define IDC_COLOR_S                     1028
#define IDC_COLOR_L                     1029
#define IDC_COLOR_HEX                   1030
#define IDC_FRAME2                      1031
#define IDC_NEW_COLOR                   1032
#define IDC_NEW_COLOR2                  1033
#define IDC_CURRENT_COLOR               1033
#define IDC_BMP_COMBINE                 32771
#define IDC_MYHELP                      32772
#define IDS_INVALIDSKINPATH             61216
#define IDS_CONFIRMSKINPATH             61217
#define IDS_EXISTSKINPATH               61218

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
