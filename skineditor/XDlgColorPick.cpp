// XDlgColorPick.cpp : 实现文件
//

#include "stdafx.h"
#include "skineditor.h"
#include "XDlgColorPick.h"
#include "afxdialogex.h"


#define CCTRL_ID	9000

// XDlgColorPick 对话框

IMPLEMENT_DYNAMIC(XDlgColorPick, CDialog)

XDlgColorPick::XDlgColorPick(
	COLORREF crCurrent,
	COLORREF crStartColor,
	COLORREF crEndColor,
	CWnd* pParent)
	: CDialog(IDD_COLOR_PICKER, pParent),
	m_crCurrent(crCurrent),
	m_crNew(crCurrent),
	m_crStartColor(crStartColor),
	m_crEndColor(crEndColor),
	m_eFormat(CXColorSpectrumCtrl::XCOLOR_TOOLTIP_NONE),
	m_pParent(pParent),
	m_bInitDone(FALSE)
{
	m_strHexColor = _T("000000");
	m_nRed = m_nGreen = m_nBlue = 0;
	m_nHue = m_nSat = m_nLum = 0;

	m_brushNew.CreateSolidBrush(m_crNew);
	m_brushCurrent.CreateSolidBrush(m_crCurrent);
}

XDlgColorPick::~XDlgColorPick()
{
	if (m_brushNew.GetSafeHandle())
		m_brushNew.DeleteObject();
	if (m_brushCurrent.GetSafeHandle())
		m_brushCurrent.DeleteObject();
}

COLORREF XDlgColorPick::GetColor()
{
	return m_crNew;
}

void XDlgColorPick::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_COLOR_R, m_nRed);
	DDX_Text(pDX, IDC_COLOR_G, m_nGreen);
	DDX_Text(pDX, IDC_COLOR_B, m_nBlue);
	DDX_Text(pDX, IDC_COLOR_H, m_nHue);
	DDX_Text(pDX, IDC_COLOR_S, m_nSat);
	DDX_Text(pDX, IDC_COLOR_L, m_nLum);
	DDX_Text(pDX, IDC_COLOR_HEX, m_strHexColor);
	DDX_Control(pDX, IDC_COLOR_R, m_editRed);
	DDX_Control(pDX, IDC_COLOR_G, m_editGreen);
	DDX_Control(pDX, IDC_COLOR_B, m_editBlue);
}


BEGIN_MESSAGE_MAP(XDlgColorPick, CDialog)
	ON_BN_CLICKED(IDOK, &XDlgColorPick::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &XDlgColorPick::OnBnClickedCancel)

	ON_REGISTERED_MESSAGE(WM_XCOLORBASIC_SELCHANGE, &XDlgColorPick::OnBasicSelChange)
	ON_REGISTERED_MESSAGE(WM_XCOLORPICKER_SELCHANGE, &XDlgColorPick::OnSelChange)
	ON_REGISTERED_MESSAGE(WM_XCOLORPICKER_SELENDOK, &XDlgColorPick::OnSelendOk)
	ON_EN_CHANGE(IDC_COLOR_R, &XDlgColorPick::OnEnChangeColorR)
	ON_EN_CHANGE(IDC_COLOR_G, &XDlgColorPick::OnEnChangeColorG)
	ON_EN_CHANGE(IDC_COLOR_B, &XDlgColorPick::OnEnChangeColorB)
	ON_EN_CHANGE(IDC_COLOR_H, &XDlgColorPick::OnEnChangeColorH)
	ON_EN_CHANGE(IDC_COLOR_S, &XDlgColorPick::OnEnChangeColorS)
	ON_EN_CHANGE(IDC_COLOR_L, &XDlgColorPick::OnEnChangeColorL)
	ON_EN_CHANGE(IDC_COLOR_HEX, &XDlgColorPick::OnEnChangeColorHex)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// XDlgColorPick 消息处理程序


void XDlgColorPick::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnOK();
}


void XDlgColorPick::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialog::OnCancel();
}


BOOL XDlgColorPick::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect rect_b;
	GetDlgItem(IDC_FRAME2)->GetWindowRect(&rect_b);
	ScreenToClient(&rect_b);
	GetDlgItem(IDC_FRAME2)->ShowWindow(SW_HIDE);
	VERIFY(m_ColorBasic.Create(AfxGetInstanceHandle(), WS_CHILD | WS_VISIBLE | WS_TABSTOP /*| WS_BORDER*/,
		rect_b, m_hWnd, CCTRL_ID+1, m_crCurrent));
// 	::SetWindowPos(m_ColorBasic.m_hWnd, ::GetDlgItem(m_hWnd, IDC_FRAME2),
// 		0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	CRect rect;
	GetDlgItem(IDC_FRAME)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	GetDlgItem(IDC_FRAME)->ShowWindow(SW_HIDE);
	VERIFY(m_ColorSpectrum.Create(AfxGetInstanceHandle(), WS_CHILD | WS_VISIBLE | WS_TABSTOP /*| WS_BORDER*/,
		rect, m_hWnd, CCTRL_ID, m_crCurrent));

	// call SetWindowPos to insert control in proper place in tab order
	::SetWindowPos(m_ColorSpectrum.m_hWnd, ::GetDlgItem(m_hWnd, IDC_FRAME),
		0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	m_ColorSpectrum.SetTooltipFormat(m_eFormat);

	SetValues();

	m_bInitDone = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}

//=============================================================================
void XDlgColorPick::SetValues()
//=============================================================================
{
	m_crNew = m_ColorSpectrum.GetRGB();
	m_nRed = GetRValue(m_crNew);
	m_nGreen = GetGValue(m_crNew);
	m_nBlue = GetBValue(m_crNew);

	char colorHex[12] = "";
	m_strHexColor = m_ColorSpectrum.GetHex(colorHex);
	BYTE h, s, l;
	m_ColorSpectrum.GetHSL(&h, &s, &l);
	m_nHue = h;
	m_nSat = s;
	m_nLum = l;

	UpdateData(FALSE);

	if (m_pParent && IsWindow(m_pParent->m_hWnd))
		m_pParent->SendMessage(WM_XCOLORPICKER_SELCHANGE, (WPARAM)m_crNew,
		(LPARAM)CCTRL_ID);

	UpdateWindow();
}

void XDlgColorPick::RefreshNewColor()
{
	CRect rect;
	GetDlgItem(IDC_NEW_COLOR)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, FALSE);
}

LRESULT XDlgColorPick::OnBasicSelChange(WPARAM wParam, LPARAM lParam)
{
	m_crNew = m_ColorBasic.GetRGB();

	m_nRed = GetRValue(m_crNew);
	m_nGreen = GetGValue(m_crNew);
	m_nBlue = GetBValue(m_crNew);

	m_ColorSpectrum.SetRGB(m_crNew);

	char colorHex[12] = "";
	m_strHexColor = m_ColorBasic.GetHex(colorHex);
	BYTE h, s, l;
	m_ColorSpectrum.GetHSL(&h, &s, &l);
	m_nHue = h;
	m_nSat = s;
	m_nLum = l;

	UpdateData(FALSE);

	if (m_brushNew.GetSafeHandle())
		m_brushNew.DeleteObject();
	m_brushNew.CreateSolidBrush(m_crNew);

	RefreshNewColor();

	if (m_pParent && IsWindow(m_pParent->m_hWnd))
		m_pParent->SendMessage(WM_XCOLORBASIC_SELCHANGE, wParam, lParam);
	return 0;
}

//=============================================================================
// handler for WM_XCOLORPICKER_SELCHANGE
LRESULT XDlgColorPick::OnSelChange(WPARAM wParam, LPARAM lParam)
//=============================================================================
{
	TRACE(_T("XDlgColorPick::OnSelChange: color=0x%06X  id=%d\n"), wParam, lParam);
	SetValues();

	if (m_brushNew.GetSafeHandle())
		m_brushNew.DeleteObject();
	m_brushNew.CreateSolidBrush(m_crNew);

	RefreshNewColor();

	if (m_pParent && IsWindow(m_pParent->m_hWnd))
		m_pParent->SendMessage(WM_XCOLORPICKER_SELCHANGE, wParam, lParam);
	return 0;
}

//=============================================================================
// handler for WM_XCOLORPICKER_SELENDOK
//=============================================================================
LRESULT XDlgColorPick::OnSelendOk(WPARAM wParam, LPARAM lParam)
{
	TRACE(_T("XDlgColorPick::OnSelendOk:  color=0x%06X  id=%d\n"), wParam, lParam);

	SetValues();
	if (m_pParent && IsWindow(m_pParent->m_hWnd))
		m_pParent->SendMessage(WM_XCOLORPICKER_SELENDOK, wParam, lParam);
	return 0;
}

//=============================================================================
void XDlgColorPick::OnChangeEdit(int type)
//=============================================================================
{
	TRACE(_T("in CTabCustom::OnChangeEdit\n"));
	if (m_bInitDone)
	{
		UpdateData(TRUE);
		{
			if (type == 0)
			{
				// RGB
				m_crNew = RGB(m_nRed, m_nGreen, m_nBlue);
				m_ColorSpectrum.SetRGB(m_crNew);
				BYTE h, s, l;
				m_ColorSpectrum.GetHSL(&h, &s, &l);
				m_nHue = h;
				m_nSat = s;
				m_nLum = l;
				char colorHex[12] = "";
				m_strHexColor = m_ColorSpectrum.GetHex(colorHex);
			}
			else if (type == 1)
			{
				// HSL
				m_ColorSpectrum.SetHSL((BYTE)m_nHue, (BYTE)m_nSat, (BYTE)m_nLum);
				m_crNew = m_ColorSpectrum.GetRGB();
				m_nRed = GetRValue(m_crNew);
				m_nGreen = GetGValue(m_crNew);
				m_nBlue = GetBValue(m_crNew);
				char colorHex[12] = "";
				m_strHexColor = m_ColorSpectrum.GetHex(colorHex);
			}
			else
			{
				m_ColorSpectrum.SetHex((LPCTSTR)m_strHexColor);
				BYTE h, s, l;
				m_ColorSpectrum.GetHSL(&h, &s, &l);
				m_nHue = h;
				m_nSat = s;
				m_nLum = l;
			}
			UpdateData(FALSE);

			OnSelChange((WPARAM)m_crNew, (LPARAM)CCTRL_ID);

			if (m_pParent&& IsWindow(m_pParent->m_hWnd))
				m_pParent->SendMessage(WM_XCOLORPICKER_SELCHANGE,
				(WPARAM)m_crNew, (LPARAM)CCTRL_ID);
		}
	}
}

BOOL XDlgColorPick::OnHelpInfo(HELPINFO * pHelpInfo)
{
	return 0;
}

BOOL XDlgColorPick::LimitEditValue(int nId, int min, int max)
{
	CString strTmp;
	int n = GetDlgItemInt(nId);
	GetDlgItemText(nId, strTmp);
	if (strTmp.GetLength() == 0)
		return FALSE;

	if (n > max)
		SetDlgItemInt(nId, max);
	if (n < min)
		SetDlgItemInt(nId, min);
	return TRUE;
}

void XDlgColorPick::OnEnChangeColorR()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	if (LimitEditValue(IDC_COLOR_R, 0, 255))
		OnChangeEdit(0);
}


void XDlgColorPick::OnEnChangeColorG()
{
	if (LimitEditValue(IDC_COLOR_G, 0, 255))
		OnChangeEdit(0);
}


void XDlgColorPick::OnEnChangeColorB()
{
	if (LimitEditValue(IDC_COLOR_B, 0, 255))
		OnChangeEdit(0);
}


void XDlgColorPick::OnEnChangeColorH()
{
	// 239
	if (LimitEditValue(IDC_COLOR_H, 0, 239))
		OnChangeEdit(1);
}


void XDlgColorPick::OnEnChangeColorS()
{
	//240
	if (LimitEditValue(IDC_COLOR_S, 0, 240))
		OnChangeEdit(1);
}


void XDlgColorPick::OnEnChangeColorL()
{
	//240
	if (LimitEditValue(IDC_COLOR_L, 0, 240))
		OnChangeEdit(1);
}


void XDlgColorPick::OnEnChangeColorHex()
{
	OnChangeEdit(2);
}


HBRUSH XDlgColorPick::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性

	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (GetDlgItem(IDC_NEW_COLOR)->m_hWnd == pWnd->m_hWnd)
		{
			if (m_brushNew.GetSafeHandle())
				hbr = m_brushNew;
		}
		else if (GetDlgItem(IDC_CURRENT_COLOR)->m_hWnd == pWnd->m_hWnd)
		{
			if (m_brushCurrent.GetSafeHandle())
				hbr = m_brushCurrent;
		}
	}

	return hbr;
}
