#pragma once

#include "colorPicker/XColorSpectrumCtrl.h"
#include "colorPicker/XColourBasicCtrl.h"
#include "afxwin.h"

// XDlgColorPick 对话框

class XDlgColorPick : public CDialog
{
	DECLARE_DYNAMIC(XDlgColorPick)

public:
	XDlgColorPick(COLORREF crCurrent,
		COLORREF crStartColor,
		COLORREF crEndColor,
		CWnd* pParent = NULL);   // 标准构造函数
	virtual ~XDlgColorPick();


	COLORREF GetColor();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_COLOR_PICKER };
#endif

public:
	CXColorSpectrumCtrl m_ColorSpectrum;
	XColorBasic m_ColorBasic;

	int m_nRed;
	int m_nGreen;
	int m_nBlue;
	int m_nHue;
	int m_nSat;
	int m_nLum;
	CString m_strHexColor;

	CEdit m_editRed;
	CEdit m_editGreen;
	CEdit m_editBlue;

protected:
	CWnd		*m_pParent;
	COLORREF	m_crCurrent;
	COLORREF	m_crNew;
	COLORREF	m_crStartColor;
	COLORREF	m_crEndColor;
	CXColorSpectrumCtrl::TOOLTIP_FORMAT m_eFormat;

	CBrush		m_brushNew;
	CBrush		m_brushCurrent;

	BOOL		m_bInitDone;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	void SetValues();
	void RefreshNewColor();
	LRESULT OnBasicSelChange(WPARAM wParam, LPARAM lParam);
	LRESULT OnSelChange(WPARAM wParam, LPARAM lParam);
	LRESULT OnSelendOk(WPARAM wParam, LPARAM lParam);
	void OnChangeEdit(int type);
	BOOL LimitEditValue(int nId, int min, int max);

	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnEnChangeColorR();
	afx_msg void OnEnChangeColorG();
	afx_msg void OnEnChangeColorB();
	afx_msg void OnEnChangeColorH();
	afx_msg void OnEnChangeColorS();
	afx_msg void OnEnChangeColorL();
	afx_msg void OnEnChangeColorHex();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

};
